﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_Manpreet_N01314914
{
    public class Reciept
    {
        public Customer cust;
        public Room room;

        public Reciept(Customer c, Room r)
        {
            cust = c;
            room = r;
        }

        public string PrintReceipt()
        {
            string message = "Hotel Booking Details: <br />";
            message += "Name: " + cust.customerName + "<br />";
            message += "Address: " + cust.customerAddress + "<br />";
            message += "Email: " + cust.customerEmail + "  Phone : " + cust.customerPhone + "<br />";
            message += "CheckIn Date: " + room.CheckInDate.ToShortDateString() + "  CheckOut Date: " + room.CheckOutDate.ToShortDateString() + "<br />";

            if (room.RoomService.Count() > 0)
            {
                message += "Service included : " + string.Join(",", room.RoomService.ToArray()) + "<br />";
            }

            message += "<br/>";
            message += "Your total is $" + CalculateBillTotal().ToString() + "<br/> Pay via :" + room.PaymentType;
            
            return message;
        }

        public double CalculateBillTotal()
        {
            double total = 0;
            if (room.RoomSize == "S")
            {
                total = 5;
            }
            else if (room.RoomSize == "M")
            {
                total = 8;
            }
            else if (room.RoomSize == "L")
            {
                total = 12;
            }

            if (room.RoomService.Count() > 0)
            {
                total = total + (room.RoomService.Count() * 2);
            }

            return total;
        }
    }
}