﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_Manpreet_N01314914
{
    public partial class HotelBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submitted(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                //if there is any data invalid or there is any exception, we will stop further processing and throw the appropriate exception
                return;
            }


            string name = customerName.Text.ToString();
            string addr = customerAddress.Text.ToString();
            string phone = customerPhoneNumber.Text.ToString();
            string email = customerEmail.Text.ToString();

            string gender;
            if (customerGenderFemale.Checked)
            {
                gender = "Female";
            }
            else
            {
                gender = "Male";
            }

            Customer cust = new Customer(name, addr, phone, email, gender);

            DateTime checkin = DateTime.Parse(roomCheckIn.Text);
            DateTime checkout = DateTime.Parse(roomCheckOut.Text);
            string size = roomSize.SelectedValue.ToString();

            List<string> service = new List<string>();

            foreach (Control con in roomService.Controls)
            {
                if (con.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)con;
                    if (cb.Checked)
                    {
                        service.Add(cb.Text);
                    }
                }
            }


            string payment;
            if (PaymentCash.Checked)
            {
                payment = "Cash";
            }
            else
            {
                payment = "Card Payment";
            }

            Room room = new Room();
            room.CheckInDate = checkin;
            room.CheckOutDate = checkout;
            room.RoomSize = size;
            room.RoomService = service;
            room.PaymentType = payment;

            Reciept reciept = new Reciept(cust, room);

            Receipt.InnerHtml = reciept.PrintReceipt();
        }
    }
}