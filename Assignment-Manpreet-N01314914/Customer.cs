﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_Manpreet_N01314914
{
    public class Customer
    {
        public string customerName;
        public string customerAddress;
        public string customerPhone;
        public string customerEmail;
        public string customerGender;

        public Customer(string name, string addr, string phone, string email, string gender)
        {
            customerAddress = addr;
            customerEmail = email;
            customerGender = gender;
            customerName = name;
            customerPhone = phone;
        }
    }
}