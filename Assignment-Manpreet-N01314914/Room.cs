﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_Manpreet_N01314914
{
    public class Room
    {
        private DateTime checkindate;
        private DateTime checkoutdate;
        private string roomsize;
        private List<string> roomservice;
        private string paymenttype;

        public string RoomSize {
            get { return roomsize; }
            set { roomsize = value; }
        }

        public List<string> RoomService {
            get { return roomservice; }
            set { roomservice = value; }
        }

        public DateTime CheckInDate {
            get { return checkindate; }
            set { checkindate = value; }
        }

        public DateTime CheckOutDate {
            get { return checkoutdate; }
            set { checkoutdate = value; }
        }

        public string PaymentType {
            get { return paymenttype; }
            set { paymenttype = value; }
        }
    }
}