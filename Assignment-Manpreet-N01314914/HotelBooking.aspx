﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="Assignment_Manpreet_N01314914.HotelBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="HotelBookingForm" Text="Hotel Booking Form" runat="server" ></asp:Label>
        <br />
        <br />
        <asp:Label runat="server">Name</asp:Label>
        <asp:TextBox ID="customerName" runat="server" placeholder="Enter Name"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="customerNameRequired" ControlToValidate="customerName" ErrorMessage="Enter Customer Name"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label runat="server">Address</asp:Label>
        <asp:TextBox ID="customerAddress" runat="server" placeholder="Enter Address"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="customerAddressRequired" ControlToValidate="customerAddress" ErrorMessage="Enter Customer Address"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label runat="server">Phone Number</asp:Label>
        <asp:TextBox ID="customerPhoneNumber" runat="server" placeholder="Enter Phone Number"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="customerPhoneNumberRequired" ControlToValidate="customerPhoneNumber" ErrorMessage="Enter Customer Phone number"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label runat="server">Email</asp:Label>
        <asp:TextBox ID="customerEmail" runat="server" placeholder="Enter Email"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="customerEmailRequired" ControlToValidate="customerEmail" ErrorMessage="Enter Customer Email"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator  runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ID="customerEmailValidate" ControlToValidate="customerEmail" ErrorMessage="Enter Correct Email"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Label runat="server" >Gender </asp:Label> 
        <asp:RadioButton ID="customerGenderMale" runat="server" GroupName="customerGender" Text="Male" />
        <asp:RadioButton ID="customerGenderFemale" runat="server" GroupName="customerGender" Text="Female" />
        <br />
        <br />
        <asp:Label runat="server">Check In Date: </asp:Label>
        <asp:TextBox runat="server" ID="roomCheckIn" placeholder="dd-mm-yyyy"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validateCheckIn" runat="server" ErrorMessage="Check In Date Required"  ControlToValidate="roomCheckIn"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label runat="server">Check Out Date: </asp:Label>
        <asp:TextBox runat="server" ID="roomCheckOut" placeholder="dd-mm-yyyy"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validateCheckOut" runat="server" ErrorMessage="Check Out Date Required" ControlToValidate="roomCheckOut" ></asp:RequiredFieldValidator>
           
        <br />
        <br />
        <asp:Label runat="server">Room Size </asp:Label>
        <asp:DropDownList ID="roomSize" runat="server">
                <asp:ListItem Value="S">Small</asp:ListItem>
                <asp:ListItem Value="M">Medium</asp:ListItem>
                <asp:ListItem Value="L">large</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label runat="server">Room Service</asp:Label>
        <div id="roomService" runat="server">
        <asp:CheckBox ID="Breakfast" runat="server" Text="Breakfast" />
        <asp:CheckBox ID="Lunch" runat="server" Text="Lunch" />
        <asp:CheckBox ID="Dinner" runat="server" Text="Dinner" />
         </div>
        <asp:Label runat="server">Payment</asp:Label>
        <asp:RadioButton runat="server" Text="Cash" GroupName="payment" ID="PaymentCash" />
        <asp:RadioButton runat="server" Text="Card Payment" GroupName="payment" ID="PaymentCard" />
        <br />
        <br />
        <asp:Button runat="server" ID="Button" OnClick="Submitted" Text="Submit"/>
        <br />
        <br />
        <asp:ValidationSummary ID="validationSummary" runat="server" HeaderText="Validation summary" />

        <div runat="server" id="Receipt"></div>
    </form>
</body>
</html>
